﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Advanced_Combat_Tracker;
using System.Text.RegularExpressions;

namespace ACTPlugins.FFXIV.Raids
{
    public class FF14SavagePlugin : IActPluginV1
    {
        private Regex regex = null;
        private string lastLine = null;

        public void InitPlugin(System.Windows.Forms.TabPage pluginScreenSpace, System.Windows.Forms.Label pluginStatusText)
        {
            ActGlobals.oFormActMain.OnLogLineRead += OnLogLineRead;
            string pattern = @".*:(.*) suffers? the effect of (Positive|Negative) Charge";
            regex = new Regex(pattern);
        }

        public void DeInitPlugin()
        {
            ActGlobals.oFormActMain.OnLogLineRead -= OnLogLineRead;
        }

        private void OnLogLineRead(bool isImport, LogLineEventArgs logInfo)
        {
            string line = logInfo.logLine;
            if (!logInfo.inCombat || !line.EndsWith("Charge."))
            {
                lastLine = null;
                return;
            }

            Match match = regex.Match(line);
            if (match.Success)
            {
                if (lastLine == null)
                {
                    lastLine = line;
                    return;
                }

                Match match1 = regex.Match(lastLine);

                string player1 = match1.Groups[1].Value;
                string player2 = match.Groups[1].Value;

                string charge1 = match1.Groups[2].Value;
                string charge2 = match.Groups[2].Value;

                if (player1 == "You" || player2 == "You")
                {
                    string partner = (player1 == "You") ? player2 : player1;
                    string tts = (charge1 == charge2 ? "Same" : "Opposite") + " Charge to " + partner;

                    ActGlobals.oFormActMain.TTS(tts);
                }
            }

            lastLine = null;
        }
    }
}
